rm -r textures/target
rm -r textures/temp
mkdir "textures/temp"
mkdir "textures/target"
mkdir "textures/target"

#latlong convert to 6 faces
./cmft/cmft --input textures/source/skybox.hdr --outputNum 1 --output0 textures/temp/face --output0params hdr,rgbe,facelist

#faces convert, so, top in +Z, bottom in -Z in ONE texture
./cmft/cmft \
--inputFacePosX textures/temp/face_posx.hdr  \
--inputFaceNegX textures/temp/face_negx.hdr  \
--inputFacePosY textures/temp/face_negz.hdr \
 --inputFaceNegY textures/temp/face_posz.hdr  \
 --inputFacePosZ textures/temp/face_posy.hdr  \
 --inputFaceNegZ textures/temp/face_negy.hdr  \
 --outputNum 1 --output0 textures/temp/hcross --output0params hdr,rgbe,hcross --posYrotate180 --negZrotate180 --posXrotate270 --negXrotate90 --posXflipH --negXflipH

#build skybox as tga
./cmft/cmft --input textures/temp/hcross.hdr --outputNum 3 --outputGamma 0.5 \
 --output0 textures/temp/skybox --output0params hdr,rgbe,hcross \
 --output1 textures/target/skybox --output1params ktx,rgb32f,cubemap \
 --output2 textures/target/skybox --output2params tga,bgr8,facelist

#build irradiance
./cmft/cmft --input textures/temp/hcross.hdr --outputNum 3 --filter irradiance \
--output0 textures/temp/irradiance --output0params hdr,rgbe,hcross \
--output1 textures/target/irradiance --output1params ktx,rgb32f,cubemap \
--output2 textures/target/irradiance --output2params tga,bgr8,facelist

#
#build spherical harmonics coefficients
#./cmft/cmft --input textures/temp/hcross.hdr --outputNum 1 --filter shc --output0 textures/target/shc --output0params tga,bgr8,hcross

#build radiance
./cmft/cmft --input textures/temp/hcross.hdr \
         --filter radiance \
         --excludeBase true \
         --srcFaceSize 256 \
         --mipCount 9 \
         --glossScale 10 \
         --glossBias 1 \
         --lightingModel blinnbrdf \
         --dstFaceSize 256 \
         --numCpuProcessingThreads 6 \
         --useOpenCL true \
         --clVendor anyGpuVendor \
         --deviceType gpu \
         --deviceIndex 0 \
         --outputNum 2 \
         --output0 textures/target/radiance \
         --output0params ktx,rgb32f,cubemap \
         --output1 textures/temp/radiance \
         --output1params hdr,rgbe,hcross

./magick/magick textures/target/irradiance_negx.tga  -quality 99   -orient TopLeft -flip textures/target/irradiance_negx.jpg
./magick/magick textures/target/irradiance_posx.tga  -quality 99   -orient TopLeft -flip textures/target/irradiance_posx.jpg
./magick/magick textures/target/irradiance_negy.tga  -quality 99   -orient TopLeft -flip textures/target/irradiance_negy.jpg
./magick/magick textures/target/irradiance_posy.tga  -quality 99   -orient TopLeft -flip textures/target/irradiance_posy.jpg
./magick/magick textures/target/irradiance_negz.tga  -quality 99   -orient TopLeft -flip textures/target/irradiance_negz.jpg
./magick/magick textures/target/irradiance_posz.tga  -quality 99   -orient TopLeft -flip textures/target/irradiance_posz.jpg

rm textures/target/irradiance_negx.tga
rm textures/target/irradiance_posx.tga
rm textures/target/irradiance_negy.tga
rm textures/target/irradiance_posy.tga
rm textures/target/irradiance_negz.tga
rm textures/target/irradiance_posz.tga

./magick/magick textures/target/skybox_negx.tga  -quality 99   -orient TopLeft -flip textures/target/skybox_negx.jpg
./magick/magick textures/target/skybox_posx.tga  -quality 99   -orient TopLeft -flip textures/target/skybox_posx.jpg
./magick/magick textures/target/skybox_negy.tga  -quality 99   -orient TopLeft -flip textures/target/skybox_negy.jpg
./magick/magick textures/target/skybox_posy.tga  -quality 99   -orient TopLeft -flip textures/target/skybox_posy.jpg
./magick/magick textures/target/skybox_negz.tga  -quality 99   -orient TopLeft -flip textures/target/skybox_negz.jpg
./magick/magick textures/target/skybox_posz.tga  -quality 99   -orient TopLeft -flip textures/target/skybox_posz.jpg

rm textures/target/skybox_negx.tga
rm textures/target/skybox_posx.tga
rm textures/target/skybox_negy.tga
rm textures/target/skybox_posy.tga
rm textures/target/skybox_negz.tga
rm textures/target/skybox_posz.tga