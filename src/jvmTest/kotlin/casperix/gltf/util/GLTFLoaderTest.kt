package casperix.gltf.util

import casperix.gltf.loader.GLTFLoaderExt
import casperix.gltf.util.GDXTestsExecutor
import org.junit.Before
import org.junit.Test

class GLTFLoaderTest {

	@Before
	fun start() {
		//	todo: load all test with reflection
		GDXTestsExecutor(listOf(::testLoader))
	}

	@Test
	fun testLoader() {
		val testAsset = GLTFLoaderExt.load("test.gltf")
		println(testAsset)
	}
}

