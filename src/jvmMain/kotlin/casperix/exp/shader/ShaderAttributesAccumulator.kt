package casperix.exp.shader

import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.Attributes
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Renderable

class ShaderAttributesAccumulator(val onAttributes: (Attributes, VertexAttributes) -> Unit) {
	private var currentVertexAttributes:VertexAttributes? = null
	private var currentMaterial: Material? = null
	private var currentEnvironment: Environment? = null
	private var currentAttributes = Attributes()

	fun push(renderable: Renderable) {
		val nextEnvironment = renderable.environment
		val nextMaterial = renderable.material
		val vertexAttributes = renderable.meshPart.mesh.vertexAttributes

		if (currentEnvironment != nextEnvironment || currentMaterial != nextMaterial || currentVertexAttributes != vertexAttributes) {
			currentEnvironment = nextEnvironment
			currentMaterial = nextMaterial
			currentVertexAttributes = vertexAttributes

			val attributes = Attributes()
			if (nextEnvironment != null) attributes.set(nextEnvironment)
			if (nextMaterial != null) attributes.set(nextMaterial)

			currentAttributes = attributes
			onAttributes(attributes, vertexAttributes)
		}
	}

	fun end() {
		currentMaterial = null
		currentEnvironment = null
	}

}