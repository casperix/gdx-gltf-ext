package casperix.exp.shader.location

import casperix.exp.shader.AbstractLocation
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.graphics.glutils.ShaderProgram

class FloatLocation<Source>(program: ShaderProgram, name: String, val provider: (source: Source) -> Float?) : AbstractLocation<Source>(program, name) {

	override fun set(renderContext: RenderContext, source: Source) {
		if (locationId == -1) return
		val value = provider(source) ?: return
		program.setUniformf(locationId, value)
	}
}