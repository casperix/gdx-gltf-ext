package casperix.exp.shader

import casperix.math.vector.Vector2i
import casperix.misc.toString
import casperix.util.ConsoleMagic
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g3d.Attribute
import com.badlogic.gdx.graphics.g3d.Attributes
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.graphics.glutils.ShaderProgram

object ShaderProgramBuilder {
	fun build(defines: Set<String>, basePath: String): ShaderProgram {
		val defineBlock = "#version 330 core\n" + defines.map { "#define $it" }.joinToString("\n", postfix = "\n")

		val vertexShader = 			defineBlock + Gdx.files.internal("$basePath.vert").readString()
		val fragmentShader = defineBlock + Gdx.files.internal("$basePath.frag").readString()

		val program = ShaderProgram(
			vertexShader,
			fragmentShader,
		)

		if (!program.isCompiled) {
			val lines = program.log.split("\n")
			val title = lines.first()
			val originalError = lines.subList(1, lines.size).joinToString("\n")

			if (title.startsWith("Fragment")) {
				printShaderError(program.fragmentShaderSource, originalError)
			} else {
				printShaderError(program.vertexShaderSource, originalError)
			}
		}
		return program
	}

	private fun printShaderError(source: String, error: String) {
		val errorPosition = parseErrorPosition(error) ?: Vector2i(-1)

		val output = source.split("\n").flatMapIndexed { lineIndex, line ->
			val lineIndexFormatted = lineIndex.toString(3, ' ')

			if (lineIndex != errorPosition.y - 1) {
				listOf(ConsoleMagic.makeWhite(lineIndexFormatted) +"  $line")
			} else {
				listOf(ConsoleMagic.makeRed(lineIndexFormatted) + ConsoleMagic.makeRed("  $line <<<"), "", ConsoleMagic.makeRed(error), "")
			}
		}
		println(output.joinToString("\n"))
	}

	private fun parseErrorPosition(error: String): Vector2i? {
		val errorFirstLine = error.split(Regex("\n"), 2).firstOrNull() ?: ""
		val item = Regex("\\d+:\\d+").find(errorFirstLine)
		if (item != null) {
			val numbers = item.value.split(":").map { Integer.parseInt(it) }
			if (numbers.size != 2) return null
			return Vector2i(numbers[0], numbers[1])
		}
		return null
	}
}

class ShaderEntry(val program: ShaderProgram, val cameraLocations: List<ShaderLocation<Camera>>, val attributesLocations: List<ShaderLocation<Attributes>>, val renderableLocations: List<ShaderLocation<Renderable>>)

interface ShaderLocation<Source> {
	fun set(renderContext: RenderContext, source: Source)
}

abstract class AbstractLocation<Context>(val program: ShaderProgram, name: String) : ShaderLocation<Context> {
	val locationId = program.fetchUniformLocation(name, false)
}


inline fun <reified T : Attribute> Attributes.getAttribute(type: Long): T? {
	return get(T::class.java, type)
}
