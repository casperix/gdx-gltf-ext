package casperix.exp.shader.attribute

import com.badlogic.gdx.graphics.g3d.Attribute

data class ShadowParamsAttribute(val bias: Float, val pcf: Boolean) : Attribute(ID) {
	companion object {
		val ID = register("ShadowParams")
	}

	override fun compareTo(other: Attribute): Int {
		val delta = other.type - this.type
		return delta.toInt()
	}

	override fun copy(): Attribute {
		return ShadowParamsAttribute(bias, pcf)
	}

}