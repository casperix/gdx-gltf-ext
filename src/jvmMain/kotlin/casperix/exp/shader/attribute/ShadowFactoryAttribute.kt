package casperix.exp.shader.attribute

import com.badlogic.gdx.graphics.g3d.Attribute

data class ShadowFactoryAttribute(val receive:Boolean, val cast:Boolean) : Attribute(ID) {
	companion object {
		val ID = register("ShadowMaterial")
	}

	override fun compareTo(other: Attribute): Int {
		val delta = other.type - this.type
		return delta.toInt()
	}

	override fun copy(): Attribute {
		return ShadowFactoryAttribute(receive, cast)
	}

}