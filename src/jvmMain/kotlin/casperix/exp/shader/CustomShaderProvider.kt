package casperix.exp.shader

import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.Shader
import com.badlogic.gdx.graphics.g3d.utils.BaseShaderProvider

class CustomShaderProvider(val provider: ShaderEntryProvider, val shadowCreationPhase: Boolean) : BaseShaderProvider() {
	override fun createShader(renderable: Renderable?): Shader {
		return CustomShader(provider, shadowCreationPhase)
	}
}

