package casperix.exp.shader

import casperix.exp.shader.attribute.ShadowParamsAttribute
import casperix.exp.shader.location.*
import casperix.gltf.attribute.ShadowLightAttribute
import casperix.gltf.attribute.ShadowTextureAttribute
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g3d.Attributes
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.attributes.DirectionalLightsAttribute
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute
import com.badlogic.gdx.math.Matrix3
import com.badlogic.gdx.math.Matrix4

object ShaderEntryFactory {

	fun create(key: ShaderEntryKey, shadowCreator: Boolean): ShaderEntry {
//		val basePath = if (key.shadowCreationPhase) "shaders/simple" else "shaders/simple"
		val basePath = if (key.shadowCreationPhase) "shaders/depth" else "shaders/colored"
		val defines = mutableSetOf<String>()
		if (key.hasVertexNormal) defines += "CALCULATE_LIGHT"
		if (key.hasVertexColor) defines += "VERTEX_COLOR"

		if (key.hasNormalTexture) defines += "NORMAL_TEXTURE"
		if (key.hasDiffuseTexture) defines += "DIFFUSE_TEXTURE"
		if (key.hasDiffuseTexture && key.blend) defines += "ALPHA_BLEND_TEXTURE"
		if (key.hasDiffuseColor) defines += "DIFFUSE_COLOR"
		if (key.hasShadowTexture && key.receiveShadow) defines += "RECEIVE_SHADOW"
		if (key.pcf) defines += "PCF_SHADOW"
		if (key.hdr) defines += "HDR_CORRECTION"
		if (key.gamma) defines += "GAMMA_CORRECTION"
		val program = ShaderProgramBuilder.build(defines, basePath)

		val cameraLocations = mutableListOf<ShaderLocation<Camera>>()
		val attributesLocations = mutableListOf<ShaderLocation<Attributes>>()
		val renderableLocations = mutableListOf<ShaderLocation<Renderable>>()

		if (key.shadowCreationPhase) {
			renderableLocations += Matrix4Location(program, "lightModelMatrix") {
				val m1 = it.environment.getAttribute<ShadowLightAttribute>(ShadowLightAttribute.ID)?.lightSpaceMatrix?.cpy() ?: Matrix4()
				m1.mul(it.worldTransform)
			}
		} else {
			renderableLocations += Matrix4Location(program, "modelMatrix") {
				it.worldTransform
			}

			attributesLocations += Matrix4Location(program, "lightSpaceMatrix") {
				val shadowLightAttribute: ShadowLightAttribute? = it.getAttribute(ShadowLightAttribute.ID)
				shadowLightAttribute?.lightSpaceMatrix
			}
		}

		if (!shadowCreator || key.blend) {
			attributesLocations += TextureLocation(program, "u_albedo_texture") {
				val diffuseTextureAttribute: TextureAttribute? = it.getAttribute(TextureAttribute.Diffuse)
				diffuseTextureAttribute?.textureDescription
			}
		}

		if (!shadowCreator) {
			cameraLocations += Matrix4Location(program, "projectionViewMatrix") {
				val p = it.projection.cpy()
				val v = it.view.cpy()
				p.mul(v)
			}

			renderableLocations += Matrix3Location(program, "normalMatrix") {
				val temp = Matrix3()
				temp.set(it.worldTransform)
				val normalMatrix = temp.inv().transpose()
				normalMatrix
			}

			attributesLocations += FloatLocation(program, "bias") {
				val shadowParamsAttribute: ShadowParamsAttribute? = it.getAttribute(ShadowParamsAttribute.ID)
				shadowParamsAttribute?.bias ?: 0f
			}

			attributesLocations += TextureLocation(program, "u_normal_texture") {
				val diffuseTextureAttribute: TextureAttribute? = it.getAttribute(TextureAttribute.Normal)
				diffuseTextureAttribute?.textureDescription
			}

			attributesLocations += ColorLocation(program, "diffuseColor") {
				val diffuseColorAttribute: ColorAttribute? = it.getAttribute(ColorAttribute.Diffuse)
				diffuseColorAttribute?.color
			}

			attributesLocations += TextureLocation(program, "shadowMap") {
				val shadowTextureAttribute: ShadowTextureAttribute? = it.getAttribute(ShadowTextureAttribute.ID)
				shadowTextureAttribute?.depthTexture
			}

			attributesLocations += Vector3Location(program, "toLightDirection") {
				val lightAttributes: DirectionalLightsAttribute? = it.getAttribute(DirectionalLightsAttribute.Type)
				if (lightAttributes == null) {
					null
				} else {
					val light = lightAttributes.lights.first()
					val toLightDirection = light.direction.cpy();
					toLightDirection.scl(-1f)
					toLightDirection
				}

			}
		}
		return ShaderEntry(program, cameraLocations, attributesLocations, renderableLocations)
	}
}