package casperix.exp.shader

import casperix.exp.shader.attribute.ColorCorrectionAttribute
import casperix.exp.shader.attribute.ShadowFactoryAttribute
import casperix.exp.shader.attribute.ShadowParamsAttribute
import casperix.gltf.attribute.ShadowTextureAttribute
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.VertexAttributes.Usage
import com.badlogic.gdx.graphics.g3d.Attributes
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute

class ShaderEntryProvider {
	private val shaders = mutableMapOf<ShaderEntryKey, ShaderEntry>()

	fun getOrCreateShader(shadowCreator: Boolean, attributes: Attributes, vertexAttributes: VertexAttributes): ShaderEntry {

		val diffuseColorAttribute: ColorAttribute? = attributes.getAttribute(ColorAttribute.Diffuse)
		val diffuseTextureAttribute: TextureAttribute? = attributes.getAttribute(TextureAttribute.Diffuse)
		val normalTextureAttribute: TextureAttribute? = attributes.getAttribute(TextureAttribute.Normal)
		val shadowParamsAttribute: ShadowParamsAttribute? = attributes.getAttribute(ShadowParamsAttribute.ID)
		val colorCorrectionAttribute: ColorCorrectionAttribute? = attributes.getAttribute(ColorCorrectionAttribute.ID)
		val shadowFactoryAttribute: ShadowFactoryAttribute? = attributes.getAttribute(ShadowFactoryAttribute.ID)
		val shadowTextureAttribute: ShadowTextureAttribute? = attributes.getAttribute(ShadowTextureAttribute.ID)
		val blendingAttribute: BlendingAttribute? = attributes.getAttribute(BlendingAttribute.Type)
		val hasColor = vertexAttributes.findByUsage(Usage.ColorUnpacked)
		val hasNormal = vertexAttributes.findByUsage(Usage.Normal)


		val condition = ShaderEntryKey(
			shadowCreator,
			hasNormal != null,
			hasColor != null,
			shadowTextureAttribute != null,
			diffuseTextureAttribute != null,
			normalTextureAttribute != null,
			diffuseColorAttribute != null,
			shadowFactoryAttribute?.receive ?: true,
			shadowParamsAttribute?.pcf ?: false,
			colorCorrectionAttribute?.hdr ?: false,
			colorCorrectionAttribute?.gamma ?: false,
			blendingAttribute?.blended ?: false,
		)

		return shaders.getOrPut(condition) {
			ShaderEntryFactory.create(condition, shadowCreator)
		}
	}
}