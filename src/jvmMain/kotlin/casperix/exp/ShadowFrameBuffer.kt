package casperix.exp

import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.GL30
import com.badlogic.gdx.graphics.glutils.FrameBuffer

class ShadowFrameBuffer(width: Int, height: Int) : FrameBuffer(casperix.exp.ShadowFrameBuffer.Companion.build(width, height)) {

	companion object {
		fun build(width: Int, height: Int): FrameBufferBuilder {
			val bufferBuilder = FrameBufferBuilder(width, height)
			bufferBuilder.addDepthRenderBuffer(GL30.GL_DEPTH_COMPONENT16)

			bufferBuilder.addColorTextureAttachment(GL30.GL_R16F, GL30.GL_RED, GL20.GL_FLOAT)
			return bufferBuilder
		}
	}

}