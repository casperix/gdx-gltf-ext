package casperix.exp

import casperix.gltf.attribute.ShadowLightAttribute
import casperix.gltf.attribute.ShadowTextureAttribute
import casperix.gltf.render.RenderManager
import casperix.misc.DisposableHolder
import casperix.signals.then
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import com.badlogic.gdx.graphics.g3d.utils.TextureDescriptor
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.math.collision.BoundingBox

class ShadowRender(val renderManager: RenderManager, val light: DirectionalLight, shadowMapWidth: Int = 512, shadowMapHeight: Int = 512, shadowViewportWidth: Float = 100f, shadowViewportHeight: Float = 100f, shadowNear: Float = 0f, shadowFar: Float = 100f) : DisposableHolder() {
	private var isActive = true
	private val environment = renderManager.environment
	private var frameBuffer: ShadowFrameBuffer?
	private var camera: Camera
	private val textureDesc: TextureDescriptor<GLTexture>
	private val center = Vector3()

	init {
		renderManager.environment.add(light)
		renderManager.onPreRender.then(components) { renderShadow() }

		frameBuffer = ShadowFrameBuffer(shadowMapWidth, shadowMapHeight)
		camera = OrthographicCamera(shadowViewportWidth, shadowViewportHeight)
		camera.near = shadowNear
		camera.far = shadowFar
		textureDesc = TextureDescriptor<GLTexture>()
		textureDesc.magFilter = Texture.TextureFilter.Nearest
		textureDesc.vWrap = Texture.TextureWrap.ClampToEdge
		updateDesc()

		environment.set(ShadowTextureAttribute(textureDesc))
	}

	private fun updateDesc() {
		textureDesc.minFilter = textureDesc.magFilter
		textureDesc.uWrap = textureDesc.vWrap
		textureDesc.texture = frameBuffer!!.colorBufferTexture
	}

	private fun renderShadow() {
		if (!isActive) return

		environment.set(ShadowLightAttribute(camera.combined))

		begin()
		renderManager.renderDepth(camera)
		end()
	}

	fun setActive(value:Boolean) {
		if (isActive == value) return
		isActive = value

		if (value) {
			environment.set(ShadowTextureAttribute(textureDesc))
		} else {
			environment.remove(ShadowTextureAttribute.ID)
		}
	}

	fun setShadowMapSize(shadowMapWidth: Int, shadowMapHeight: Int): ShadowRender {
		if (frameBuffer == null || frameBuffer!!.width != shadowMapWidth || frameBuffer!!.height != shadowMapHeight) {
			if (frameBuffer != null) {
				frameBuffer!!.dispose()
			}
			frameBuffer = ShadowFrameBuffer(shadowMapWidth, shadowMapHeight)
			updateDesc()
		}
		return this
	}

	fun setViewport(shadowViewportWidth: Float, shadowViewportHeight: Float, shadowNear: Float, shadowFar: Float): ShadowRender {
		camera.viewportWidth = shadowViewportWidth
		camera.viewportHeight = shadowViewportHeight
		camera.near = shadowNear
		camera.far = shadowFar
		return this
	}

	fun setCenter(center: Vector3?): ShadowRender {
		this.center.set(center)
		return this
	}

	fun setCenter(x: Float, y: Float, z: Float): ShadowRender {
		center[x, y] = z
		return this
	}

	fun setBounds(box: BoundingBox): ShadowRender {
		var w = box.width
		var h = box.height
		var d = box.depth
		val s = Math.max(Math.max(w, h), d)
		d = s * SQRT2
		h = d
		w = h
		box.getCenter(center)
		return setViewport(w, h, 0f, d)
	}

	protected fun validate() {
		val halfDepth = camera.near + 0.5f * (camera.far - camera.near)
		camera.position.set(light.direction).scl(-halfDepth).add(center)
		camera.direction.set(light.direction).nor()
		camera.up.set(Vector3.Z)
		camera.normalizeUp()
		camera.update()
	}

	fun begin() {
		validate()
		val w = frameBuffer!!.width
		val h = frameBuffer!!.height
		frameBuffer!!.begin()
		Gdx.gl.glViewport(0, 0, w, h)
		Gdx.gl.glClearColor(1f, 1f, 1f, 1f)
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)
		Gdx.gl.glEnable(GL20.GL_SCISSOR_TEST)
		Gdx.gl.glScissor(1, 1, w - 2, h - 2)
	}

	fun end() {
		Gdx.gl.glDisable(GL20.GL_SCISSOR_TEST)
		frameBuffer!!.end()
	}

	fun getProjViewTrans(): Matrix4 {
		return camera.combined
	}

	fun getDepthMap(): TextureDescriptor<*> {
		return textureDesc
	}

	override fun dispose() {
		if (frameBuffer != null) frameBuffer!!.dispose()
		frameBuffer = null
	}

	companion object {
		protected val SQRT2 = Math.sqrt(2.0).toFloat()
	}

}
