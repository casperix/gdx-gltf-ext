package casperix.gltf.material

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL30
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.Attribute
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute
import com.badlogic.gdx.graphics.g3d.utils.TextureDescriptor
import com.badlogic.gdx.utils.Array
import net.mgsx.gltf.scene3d.attributes.PBRColorAttribute
import net.mgsx.gltf.scene3d.attributes.PBRFloatAttribute
import net.mgsx.gltf.scene3d.attributes.PBRTextureAttribute

class MaterialBuilder {
	val attributes = mutableListOf<Attribute>()

	init {
		setColor(Color.WHITE)
	}

	fun build():Material {
		val mat = Material(Array(attributes.toTypedArray()))
		return mat
	}

	fun removeIf(condition: (Attribute) -> Boolean): MaterialBuilder {
		attributes.removeIf(condition)
		return this
	}

	fun removeAll(): MaterialBuilder {
		attributes.clear()
		return this
	}

	fun add(attribute: Attribute): MaterialBuilder {
		attributes += attribute
		return this
	}

	fun setTexture(texture: Texture, type: Long = PBRTextureAttribute.BaseColorTexture): MaterialBuilder {
		attributes.removeAll { it is PBRTextureAttribute && it.type == type }
		attributes += PBRTextureAttribute(type, TextureDescriptor(texture, texture.minFilter, texture.magFilter, texture.uWrap, texture.vWrap))
		return this
	}

	fun setColor(color: Color): MaterialBuilder {
		attributes.removeAll { it is PBRColorAttribute && it.type == PBRColorAttribute.BaseColorFactor }
		attributes += PBRColorAttribute(PBRColorAttribute.BaseColorFactor, color)
		return this
	}

	fun setProperties(roughness: Double, metallic: Double): MaterialBuilder {
		attributes.removeAll { it is PBRFloatAttribute && (it.type == PBRFloatAttribute.Roughness || it.type == PBRFloatAttribute.Metallic) }
		attributes += PBRFloatAttribute(PBRFloatAttribute.Roughness, roughness.toFloat())
		attributes += PBRFloatAttribute(PBRFloatAttribute.Metallic, metallic.toFloat())
		return this
	}

	fun setCullFace(cullFront: Boolean, cullBack: Boolean): MaterialBuilder {
		val param = if (cullBack && cullFront) {
			GL30.GL_FRONT_AND_BACK
		} else if (cullBack) {
			GL30.GL_BACK
		} else if (cullFront) {
			GL30.GL_FRONT
		} else {
			0
		}

		attributes.removeAll { it is IntAttribute && (it.type == IntAttribute.CullFace) }
		attributes += IntAttribute(IntAttribute.CullFace, param)
		return this
	}

	fun setOpacity(opacity: Double): MaterialBuilder {
		attributes.removeAll { it is BlendingAttribute }

		if (opacity < 1.0) {
			attributes += BlendingAttribute(true, opacity.toFloat())
		}
		return this
	}

	fun hasTexture(material: Material): Boolean {
		material.forEach {
			if (it is PBRTextureAttribute) return true
		}
		return false
	}
}