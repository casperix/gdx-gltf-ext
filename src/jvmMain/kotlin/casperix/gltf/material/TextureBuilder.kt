package casperix.gltf.material

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Texture

object TextureBuilder {

	fun build(file: FileHandle, useMipMap: Boolean, minFilter: Texture.TextureFilter = Texture.TextureFilter.Nearest, magFilter: Texture.TextureFilter = Texture.TextureFilter.Nearest, uWrap: Texture.TextureWrap = Texture.TextureWrap.ClampToEdge, vWrap: Texture.TextureWrap = Texture.TextureWrap.ClampToEdge, anisotropicFilterLevel: Float = 1f): Texture {
		val texture = Texture(file, useMipMap)
		texture.setFilter(minFilter, magFilter)
		texture.setWrap(uWrap, vWrap)
		texture.setAnisotropicFilter(anisotropicFilterLevel)
		return texture
	}
}