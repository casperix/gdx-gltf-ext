package casperix.gltf.render

import casperix.app.WindowWatcher
import casperix.exp.shader.CustomShaderProvider
import casperix.exp.shader.ShaderEntryProvider
import casperix.gltf.attribute.DepthRenderAttribute
import casperix.misc.DisposableHolder
import casperix.gltf.scene.DefaultRenderableMap
import casperix.gltf.scene.RenderableGroup
import casperix.gltf.scene.RenderableMap
import casperix.signals.then
import casperix.math.color.Color4d
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.g3d.RenderableProvider
import com.badlogic.gdx.graphics.g3d.utils.ShaderProvider
import com.badlogic.gdx.math.Vector3
import net.mgsx.gltf.scene3d.scene.SceneManager
import net.mgsx.gltf.scene3d.shaders.PBRCommon
import net.mgsx.gltf.scene3d.shaders.PBRShaderProvider


class RenderManager(val watcher: WindowWatcher, var needClearColor: Boolean, var clearColor: Color4d, val mainShaderProvider: ShaderProvider, val shadowShaderProvider: ShaderProvider) : DisposableHolder(), RenderableMap {

	companion object {
		/**
		 * 	Renderer use PBR technic
		 */
		fun createPBRender(watcher: WindowWatcher, clearColor: Color4d = Color4d(0.0, 0.4, 0.8, 1.0)): RenderManager {
			return RenderManager(
				watcher, true, clearColor,
				PBRShaderProvider.createDefault(PBRShaderProvider.createDefaultConfig()),
				PBRShaderProvider.createDefaultDepth(PBRShaderProvider.createDefaultDepthConfig()),
			)
		}

		/**
		 * 	Renderer use simplified experimental technic
		 */
		fun createExperimentalRender(watcher: WindowWatcher, clearColor: Color4d = Color4d(0.0, 0.4, 0.8, 1.0)): RenderManager {
			val provider = ShaderEntryProvider()
			return RenderManager(
				watcher, true, clearColor,
				CustomShaderProvider(provider, false),
				CustomShaderProvider(provider, true),
			)
		}
	}

	val map = DefaultRenderableMap()

	val camera = PerspectiveCamera()
	val sceneManager = SceneManager()
	val environment = sceneManager.environment

	val modelMainBatch = ModelBatchExt(mainShaderProvider)
	val modelShadowBatch = ModelBatchExt(shadowShaderProvider)


	@Deprecated(message = "use onUpdate")
	val nextFrame = watcher.onUpdate
	val onUpdate = watcher.onUpdate
	val onPreRender = watcher.onPreRender

	init {
		camera.position.set(Vector3(-1f, -1f, 1f))
		camera.direction.set(Vector3(2f, 2f, -1f))
		camera.up.set(Vector3(0f, 0f, 1f))

		updateCameras()
		sceneManager.setCamera(camera)

		watcher.onUpdate.then(components) { update() }
		watcher.onRender.then(components) { render() }
		watcher.onResize.then(components) { resize(it.x, it.y) }
		watcher.onExit.then(components) { dispose() }
	}

	override fun dispose() {
		modelMainBatch.dispose()
		sceneManager.dispose()
	}

	private fun update() {
		val tick = Gdx.graphics.deltaTime
		sceneManager.update(tick)
	}

	fun clear(forceClearColor: Boolean) {
		val antiAliasingBits = if (Gdx.graphics.bufferFormat.coverageSampling) GL20.GL_COVERAGE_BUFFER_BIT_NV else 0
		val clearColorBits = if (needClearColor || forceClearColor) GL20.GL_COLOR_BUFFER_BIT else 0
		Gdx.gl.glClearColor(clearColor.x.toFloat(), clearColor.y.toFloat(), clearColor.z.toFloat(), clearColor.w.toFloat())
		Gdx.gl.glClear(clearColorBits or GL20.GL_DEPTH_BUFFER_BIT or antiAliasingBits)
	}


	private fun render() {
		clear(false)
		PBRCommon.enableSeamlessCubemaps()
		renderMap(modelMainBatch, camera, map.groups.filter { it.key.renderColor }.toSortedMap { A, B -> compare(A, B) })
	}

	override fun clear() {
		map.clear()
	}

	override fun add(instance: RenderableProvider, group: RenderableGroup) {
		map.add(instance, group)
	}

	override fun remove(instance: RenderableProvider, group: RenderableGroup) {
		map.remove(instance, group)
	}

	private fun compare(A: RenderableGroup, B: RenderableGroup): Int {
		val deltaPriority = B.priority - A.priority
		if (deltaPriority != 0) return deltaPriority
		if (B.renderColor != A.renderColor) return if (B.renderColor) 1 else -1
		if (B.castShadow != A.castShadow) return if (B.castShadow) 1 else -1
		return 0
	}

	fun renderDepth(camera: Camera?) {
		environment.set(DepthRenderAttribute())
		renderMap(modelShadowBatch, camera ?: this.camera, map.groups.filter { it.key.castShadow }.toSortedMap { A, B -> compare(A, B) })
		environment.remove(DepthRenderAttribute.ID)
	}

	private fun renderMap(batch: ModelBatchExt, camera: Camera, map: Map<RenderableGroup, Set<RenderableProvider>>) {
		batch.begin(camera)

		map.values.forEach { value ->
			batch.renderOptionalEnvironment(value, environment)
		}

		batch.end()
	}

	private fun updateCameras() {
		camera.update()
	}

	private fun resize(width: Int, height: Int) {
		sceneManager.updateViewport(width.toFloat(), height.toFloat())
		updateCameras()
	}
}