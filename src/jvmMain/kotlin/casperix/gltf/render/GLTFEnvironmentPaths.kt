package casperix.gltf.render

class GLTFEnvironmentPaths(
		val environmentPath:String = "textures/environment/environment_",
		val diffusePath:String = "textures/diffuse/diffuse_",
		val specularPath:String = "textures/specular/specular_",
		val brdfPath:String = "textures/brdfLUT.png",
)