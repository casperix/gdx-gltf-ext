package casperix.gltf.render

import casperix.misc.DisposableHolder
import casperix.misc.disposeAll
import casperix.gltf.scene.RenderableGroup
import casperix.gltf.scene.RenderableMap
import casperix.signals.then
import com.badlogic.gdx.graphics.g3d.ModelCache
import com.badlogic.gdx.graphics.g3d.RenderableProvider

class CachedSceneCollection(val renderManager: RenderManager, val group: RenderableGroup = RenderableGroup(true, true, 0)) : DisposableHolder(), RenderableMap {
	private val cache = ModelCache()


	private var dirty: Boolean = false
		set(value) {
			if (field == value) return
			field = value
			if (value) {
				renderManager.onUpdate.then(components) { onFrame() }
			} else {
				components.disposeAll()
			}

		}

	private val instances = mutableListOf<RenderableProvider>()

	override fun dispose() {
		super.dispose()
		clear()
	}

	override fun clear() {
		instances.clear()
		dirty = true
	}

	override fun add(instance: RenderableProvider, group: RenderableGroup) {
		instances.add(instance)
		dirty = true
	}

	override fun remove(instance: RenderableProvider, group: RenderableGroup) {
		instances.remove(instance)
		dirty = true
	}

	private fun onFrame() {
		if (!dirty) return
		rebuild()
		dirty = false
	}

	private fun rebuild() {
		if (instances.isEmpty()) {
			renderManager.remove(cache, group)
		} else {
			renderManager.add(cache, group)
			cache.begin()
			cache.add(instances)
//			MeshBuilderHack.dropIndicesMap()
			cache.end()
		}
	}
}

