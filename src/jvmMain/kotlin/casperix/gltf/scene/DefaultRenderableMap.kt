package casperix.gltf.scene

import casperix.misc.Disposable
import com.badlogic.gdx.graphics.g3d.RenderableProvider

class DefaultRenderableMap() : Disposable, RenderableMap {
	val groups = mutableMapOf<RenderableGroup, MutableSet<RenderableProvider>>()

	override fun clear() {
		groups.clear()
	}

	fun getGroupSize(group: RenderableGroup): Int {
		return getGroup(group).size
	}

	fun getGroup(group: RenderableGroup): MutableSet<RenderableProvider> {
		return groups.getOrPut(group) { mutableSetOf() }
	}

	override fun add(instance: RenderableProvider, group: RenderableGroup) {
		getGroup(group) += instance
	}

	override fun remove(instance: RenderableProvider, group: RenderableGroup) {
		getGroup(group) -= instance
	}

	override fun dispose() {
		clear()
	}

}