package casperix.gltf.imposter

import casperix.gdx.geometry.toVector3
import casperix.math.vector.Vector3d
import casperix.math.vector.Vector4d
import casperix.gltf.render.RenderManager
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.glutils.FrameBufferTextureArray
import com.badlogic.gdx.utils.ScreenUtils
import net.mgsx.gltf.scene3d.shaders.PBRShaderProvider
import org.lwjgl.opengl.GL30
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

class ImposterFactory(val renderManager: RenderManager, val environment: Environment, val model: Model, val settings: ImposterSettings) {

	class Frame(val vStep: Int, val hStep: Int, val data: Pixmap)

	private val instances = listOf(ModelInstance(model))
	private val modelBatch = ModelBatch(PBRShaderProvider.createDefault(PBRShaderProvider.createDefaultConfig()))
	private val camera = OrthographicCamera(settings.textureSize.x.toFloat(), settings.textureSize.y.toFloat())

	init {
		camera.zoom = settings.cameraZoom.toFloat() / settings.textureSize.x
		camera.near = -500f
		camera.far = 500f
		camera.viewportWidth = settings.textureSize.x.toFloat()
		camera.viewportHeight = settings.textureSize.y.toFloat()
	}

	fun buildPixmap(): List<Frame> {
		val items = mutableListOf<Frame>()
		for (vStep in 0 until settings.vertical.frames) {
			for (hStep in 0 until settings.horizontal.frames) {
				renderLayer(hStep, vStep)
				val data = ScreenUtils.getFrameBufferPixmap(0, 0, settings.textureSize.x, settings.textureSize.y)
				items += Frame(vStep, hStep, data)
				//items += buffer.colorBufferTexture.textureData.consumePixmap()
			}
		}
		return items
	}

	fun build(): ImposterModel {
		val buffer = FrameBufferTextureArray(
			settings.textureSize.x, settings.textureSize.y, settings.vertical.frames * settings.horizontal.frames,
			Pixmap.Format.RGBA8888, settings.hasDepth, settings.hasStencil,
			Texture.TextureFilter.MipMapNearestLinear, Texture.TextureFilter.Nearest, Texture.TextureWrap.ClampToEdge, Texture.TextureWrap.ClampToEdge
		)

		buffer.bind()

		for (vStep in 0 until settings.vertical.frames) {
			for (hStep in 0 until settings.horizontal.frames) {
				if (!buffer.nextSide()) throw Error("Invalid amount of texture layers")
				renderLayer(hStep, vStep)
			}
		}

		buffer.end()

		buffer.colorBufferTexture.bind()
		GL30.glGenerateMipmap(GL30.GL_TEXTURE_2D_ARRAY)

		return ImposterModel(buffer.colorBufferTexture, settings)
	}


	private fun renderLayer(hStep: Int, vStep: Int) {
		val horizontal = settings.horizontal
		val vertical = settings.vertical

		val hAngle = if (horizontal.frames == 1) horizontal.minAngle else (horizontal.maxAngle - horizontal.minAngle) * hStep / (horizontal.frames - 1).toDouble() + horizontal.minAngle
		val vAngle = if (vertical.frames == 1) vertical.minAngle else (vertical.maxAngle - vertical.minAngle) * vStep / (vertical.frames - 1).toDouble() + vertical.minAngle

		val toCamera = Vector3d(
			sin(vAngle) * cos(hAngle),
			sin(vAngle) * sin(hAngle),
			cos(vAngle)
		)

		val cameraDirection = (-toCamera).normalize()
		camera.position.set((settings.rotationCenter + toCamera).toVector3())
		camera.direction.set(cameraDirection.toVector3())

		val right = Vector3d(cos(hAngle - PI / 2.0), sin(hAngle- PI / 2.0), 0.0)
		val up = right.cross(cameraDirection).normalize()
		camera.up.set(up.toVector3())

		camera.update()
//				camera.magicUpdate()

		Gdx.gl.glViewport(0, 0, settings.textureSize.x, settings.textureSize.y)

		val originalClearColor = renderManager.clearColor
		renderManager.clearColor = Vector4d(0.0)
		renderManager.clear(true)
		renderManager.clearColor = originalClearColor

		modelBatch.begin(camera)
		modelBatch.render(instances, environment)
		modelBatch.end()
	}

}