package casperix.gltf.imposter

import casperix.math.vector.Vector2d
import casperix.math.vector.Vector3d
import casperix.gltf.material.MaterialBuilder
import casperix.gltf.util.MeshRender
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.RenderableProvider
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Pool

class ImposterRenderer() : RenderableProvider {
	class ModelLayer(val model: ImposterModel, val material: Material, val buffers: MutableList<ModelBuffer>){
		fun clear() {
			buffers.forEach {
				it.amount = 0
				it.dirty = true
			}
		}
		fun dispose() {
			buffers.forEach { it.dispose() }
			buffers.clear()
		}
	}
	class ModelBuffer(val builder: ImposterVertexBuffer, val renderer: MeshRender, var amount: Int, var dirty: Boolean) {
		fun dispose() {
			builder.mesh.dispose()
		}
	}

	private val maxParticlesInBuffer = 16384
	private val shader = ImposterShader()
	private val layers = mutableMapOf<ImposterModel, ModelLayer>()
	private var dirty = false

	fun clear() {
		layers.values.forEach { layer->
			layer.clear()
		}
		dirty = true
	}

	@Deprecated(message = "function calls automatically if needed")
	fun flush() {
		if (!dirty) return

		layers.values.forEach { layer ->
			layer.buffers.forEach { buffer ->
				if (buffer.dirty) {
					buffer.builder.flush(buffer.amount)
					buffer.dirty = false
				}
			}
		}

		dirty = false
	}

	private fun getLayer(model: ImposterModel): ModelLayer {
		return layers.getOrPut(model) {
			ModelLayer(
				model,
				MaterialBuilder()
					.add(model)
					.build(),
				mutableListOf()
			)
		}
	}

	fun addParticle(model: ImposterModel, position: Vector3d, size: Vector2d) {
		val layer = getLayer(model)
		layer.buffers.forEach {
			if (addParticle(it, position, size)) return
		}
		val vertexBuffer = ImposterVertexBuffer(maxParticlesInBuffer)
		val buffer = ModelBuffer(vertexBuffer, MeshRender(vertexBuffer.mesh, Matrix4(), layer.material, shader), 0, false)
		layer.buffers += buffer
		addParticle(buffer, position, size)
	}

	private fun addParticle(buffer: ModelBuffer, position: Vector3d, size: Vector2d): Boolean {
		if (buffer.amount < maxParticlesInBuffer) {
			buffer.builder.setParticle(buffer.amount, position + Vector3d(0.0, 0.0, size.y / 2.0), size)
			buffer.amount++
			buffer.dirty = true
			dirty = true
			return true
		} else {
			return false
		}
	}

	override fun getRenderables(renderables: Array<Renderable>?, pool: Pool<Renderable>?) {
		flush()
		layers.values.forEach { layer ->
			layer.buffers.forEach { buffer ->
				buffer.renderer.getRenderables(renderables, pool)
			}
		}
	}
}