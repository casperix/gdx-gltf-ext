package casperix.gltf.imposter

import casperix.math.vector.Vector2i
import casperix.math.vector.Vector3d

class ImposterSettings(
	val textureSize: Vector2i,
	val rotationCenter:Vector3d,
	val cameraZoom:Double,
	val horizontal: RangeInfo,
	val vertical: RangeInfo,
	val cameraUp: Vector3d,
	val hasDepth:Boolean,
	val hasStencil:Boolean,
	) {
	class RangeInfo(val minAngle:Double, val maxAngle:Double, val frames:Int) {
		init {
			if (frames <= 0) throw Error("frames must be positive number")
		}
	}

	val textureAmount get() = vertical.frames * horizontal.frames
}