package casperix.gltf.imposter

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.Shader
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import net.mgsx.gltf.scene3d.attributes.PBRFloatAttribute
import net.mgsx.gltf.scene3d.lights.DirectionalShadowLight
import kotlin.math.*

class ImposterShader : Shader {
	private val receiveShadow = false
	private val vertexShader = Gdx.files.internal("shaders/imposter.vert").readString()
	private val fragmentShader = Gdx.files.internal("shaders/imposter.frag").readString()

	private val withShadowShader = ShaderProgram(
		"#version 330" + "\n" +
				"#define shadowMapFlag" + "\n" +
				vertexShader,
		"#version 330" + "\n" +
				"#define shadowMapFlag" + "\n" +
				fragmentShader
	)

	private val defaultShader = ShaderProgram(
		"#version 330" + "\n" +
				vertexShader,
		"#version 330" + "\n" +
				fragmentShader
	)

	private val vertexShadowShader = Gdx.files.internal("shaders/imposterShadow.vert").readString()
	private val fragmentShadowShader = Gdx.files.internal("shaders/imposterShadow.frag").readString()

	private val shadowShader = ShaderProgram(
		"#version 330" + "\n" +
				vertexShadowShader,
		"#version 330" + "\n" +
				fragmentShadowShader
	)


	private var currentCamera: Camera? = null
	private var currentContext: RenderContext? = null
	private var currentRenderable: Renderable? = null
	private var currentProgram: ShaderProgram? = null
	private var currentMesh: Mesh? = null

	override fun dispose() {

	}

	override fun init() {

	}

	override fun begin(camera: Camera?, context: RenderContext?) {
		if (camera == null || context == null) return
		currentContext = context
		currentCamera = camera
	}

	private fun bind(renderable: Renderable) {
		val context = currentContext ?: return
		val camera = currentCamera ?: return
		val environment = renderable.environment ?: return
		val shadowMap: DirectionalShadowLight? = environment.shadowMap as? DirectionalShadowLight
		val attribute = renderable.material.get(ImposterModel.imposterAttribute) as? ImposterModel ?: return

		val shadowIteration = shadowMap != null && shadowMap.camera == camera
		val shader = if (shadowIteration) shadowShader else if (shadowMap == null) defaultShader else withShadowShader
		val vertical = attribute.settings.vertical
		val horizontal = attribute.settings.horizontal

		shader.bind()
		context.setCullFace(GL20.GL_FRONT)
		context.setDepthTest(GL20.GL_LEQUAL)
//		context.setDepthMask(false)


		shader.setUniformf("u_cameraPosition", camera.position)
		shader.setUniformi("u_textureArray", context.textureBinder.bind(attribute.textureArray))



		if (shadowIteration) {
			context.setBlending(false, GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA)

			val temp = camera.combined.cpy()
			temp.mul(renderable.worldTransform)
			shader.setUniformMatrix("u_projViewWorldTrans", temp)
		} else {
			context.setBlending(true, GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA)

			shader.setUniformMatrix("u_projViewTrans", camera.combined)
			if (receiveShadow && shadowMap != null) {
				val shadowTextureId = context.textureBinder.bind(shadowMap.depthMap)
				val shadowBiasAttribute = environment.get(PBRFloatAttribute.ShadowBias) as? PBRFloatAttribute
				shader.setUniformf("u_shadowBias", shadowBiasAttribute?.value ?: 0f)
				shader.setUniformMatrix("u_shadowMapProjViewTrans", shadowMap.projViewTrans)
				shader.setUniformi("u_shadowTexture", shadowTextureId)
				shader.setUniformf("u_shadowPCFOffset", 1f / (2f * shadowMap.depthMap.texture.width))
			}
		}

		shader.setUniformf("u_vertical.lastFrame", (vertical.frames - 1).toFloat())
		shader.setUniformf("u_vertical.minAngle", vertical.minAngle.toFloat())
		shader.setUniformf("u_vertical.maxAngle", vertical.maxAngle.toFloat())

		shader.setUniformf("u_horizontal.lastFrame", (horizontal.frames - 1).toFloat())
		shader.setUniformf("u_horizontal.minAngle", horizontal.minAngle.toFloat())
		shader.setUniformf("u_horizontal.maxAngle", horizontal.maxAngle.toFloat())

		currentRenderable = renderable
		currentProgram = shader
	}

	private fun render() {
		val program = currentProgram ?: return
		val mesh = currentRenderable?.meshPart?.mesh ?: return
		mesh.bind(program)
		mesh.render(program, GL20.GL_TRIANGLES, 0, mesh.numIndices, false)
		currentMesh = mesh
	}

	private fun unbind() {
		val program = currentProgram
		val mesh = currentMesh
		val context = currentContext

		if (mesh != null && program != null) {
			mesh.unbind(program)
		}


		currentMesh = null
		currentProgram = null
		currentRenderable = null
		currentContext = null
	}

	override fun render(renderable: Renderable?) {
		if (renderable == null) return
		bind(renderable)
		render()
	}

	override fun end() {
		unbind()
	}

	override fun compareTo(other: Shader?): Int {
		if (other == null) return -1
		if (other === this) return 0
		return 1
	}

	override fun canRender(instance: Renderable?): Boolean {
		return instance?.shader == this
	}
}