package casperix.gltf.imposter

import casperix.math.vector.Vector2d
import casperix.math.vector.Vector3d
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.VertexAttribute
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.glutils.ShaderProgram

class ImposterVertexBuffer(val maxParticles: Int) {
	private val vertexShader = Gdx.files.internal("shaders/imposter.vert").readString()
	private val fragmentShader = Gdx.files.internal("shaders/imposter.frag").readString()
	private val defaultShader = ShaderProgram(
		"#version 330" + "\n" +
				vertexShader,
		"#version 330" + "\n" +
				fragmentShader
	)


	private val attributes = VertexAttributes(
		VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"),
		VertexAttribute(VertexAttributes.Usage.Generic, 2, "a_offset"),
		VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, "a_tex"),
		VertexAttribute(VertexAttributes.Usage.Generic, 1, "a_some"),
	)

	private val numberInVertex = attributes.vertexSize / 4
	private val indices = ShortArray(maxParticles * 6)
	private val vertices = FloatArray((maxParticles * 4) * numberInVertex)

	val mesh = Mesh(		false, vertices.size, indices.size,attributes	)

	init {
		if (maxParticles > 16384)
			throw Error("Area must be contains not more than 65536 points")

		buildVertices()
		buildIndices()
//		mesh.setIndices(indices)
//		mesh.setVertices(vertices)
	}

	private fun buildVertices() {
		for (particleNum in 0 until maxParticles) {
			for (dx in 0..1) {
				for (dy in 0..1) {
					val offset = (particleNum * 4 + dx + dy * 2) * numberInVertex
					vertices[offset + 5] = dx.toFloat()
					vertices[offset + 6] = dy.toFloat()
				}
			}
		}
	}

	fun flush(usedParticles: Int) {
		//	its hack, for correct vertex-data update
		mesh.bind(defaultShader)

		mesh.setVertices(vertices, 0,  usedParticles * 4 * numberInVertex)

		//	its hack, for correct vertex-data update
		mesh.unbind(defaultShader)

		mesh.setIndices(indices, 0, usedParticles * 6)
	}

	fun setParticle(particleNum: Int, position: Vector3d, size: Vector2d) {
		for (dx in 0..1) {
			for (dy in 0..1) {
				val offset = (particleNum * 4 + dx + dy * 2) * numberInVertex
				vertices[offset + 0] = position.x.toFloat()
				vertices[offset + 1] = position.y.toFloat()
				vertices[offset + 2] = position.z.toFloat()
				vertices[offset + 3] = (dx.toFloat() - 0.5f) * size.x.toFloat()
				vertices[offset + 4] = (dy.toFloat() - 0.5f) * size.y.toFloat()
			}
		}
	}

	private fun buildIndices() {
		for (p in 0 until maxParticles) {
			indices[p * 6 + 0] = (p * 4 + 0).toShort()
			indices[p * 6 + 1] = (p * 4 + 1).toShort()
			indices[p * 6 + 2] = (p * 4 + 2).toShort()
			indices[p * 6 + 3] = (p * 4 + 2).toShort()
			indices[p * 6 + 4] = (p * 4 + 1).toShort()
			indices[p * 6 + 5] = (p * 4 + 3).toShort()
		}
	}
}