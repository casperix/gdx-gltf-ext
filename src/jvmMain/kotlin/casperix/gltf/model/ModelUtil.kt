package casperix.gltf.model

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.VertexAttribute
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.model.MeshPart
import com.badlogic.gdx.graphics.g3d.model.Node
import com.badlogic.gdx.graphics.g3d.model.NodePart
import com.badlogic.gdx.graphics.g3d.utils.MeshBuilder
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Quaternion
import com.badlogic.gdx.math.Vector3
import net.mgsx.gltf.scene3d.scene.SceneAsset

object ModelUtil {

	fun colorize(model: Model, color: Color): Model {
		model.materials.forEach {
			if (it.id.contains("Paint")) {
				it.set(ColorAttribute.createDiffuse(color))
			}
		}
		return model
	}


	fun mergeAssetModel(asset: SceneAsset): SceneAsset {
		mergeModel(asset.scene.model)
		return asset
	}

	fun union(base: Model, append: Model, translation: Vector3, rotation: Quaternion, scale: Vector3) {
		append.nodes.forEach {
			if (!it.parts.isEmpty || it.children.firstOrNull() != null) {
				val next = it.copy()
				next.translation.add(translation)
				next.rotation.mul(rotation)
				next.scale.set(next.scale.x * scale.x, next.scale.y * scale.y, next.scale.z * scale.z)
				next.calculateWorldTransform()
				next.calculateLocalTransform()
				base.nodes.add(next)
			}
		}
	}

	fun mergeModel(model: Model): Model {
		val partsMap = mutableMapOf<Material, MutableList<Pair<Node, NodePart>>>()

		model.nodes.forEach { node ->
			node.parts.forEach { part ->
				val parts = partsMap.getOrPut(part.material, { mutableListOf() })
				parts.add(Pair(node, part))
			}
		}

		partsMap.forEach {
			val builder = MeshBuilder()
			val material = it.key
			val partList = it.value

			partList.forEach { pair ->
				val originalNode = pair.first
				val originalPart = pair.second

				if (builder.numIndices == 0) {
					builder.begin(originalPart.meshPart.mesh.vertexAttributes, originalPart.meshPart.primitiveType)
				}

				originalNode.calculateLocalTransform()
				val originalTransform = originalNode.calculateWorldTransform()

				val mesh = originalPart.meshPart.mesh.copy(true)
				mesh.transform(originalTransform, 0, mesh.getNumVertices())

				val transformN = originalTransform.cpy()
				transformN.setTranslation(Vector3.Zero)

				transformNormal(mesh, transformN, 0, mesh.getNumVertices())

				builder.addMesh(mesh)
				model.nodes.removeValue(originalNode, true)
			}
			if (builder.numIndices != 0) {
				val node = Node()
				node.id = "generated"
				val mesh = builder.end()

				val meshPart = MeshPart()
				meshPart.mesh = mesh
				meshPart.offset = 0
				meshPart.primitiveType = builder.primitiveType
				meshPart.size = mesh.numIndices

				val rootPart = NodePart(meshPart, material)
				node.parts.add(rootPart)

				model.meshes.add(mesh)
				model.nodes.add(node)
				model.meshParts.add(meshPart)
			}

		}

		return model
	}

	fun transformNormal(mesh: Mesh, matrix: Matrix4?, start: Int, count: Int) {
		val posAttr: VertexAttribute = mesh.getVertexAttribute(VertexAttributes.Usage.Normal)
		val posOffset = posAttr.offset / 4
		val stride: Int = mesh.getVertexSize() / 4
		val numComponents = posAttr.numComponents
		val numVertices: Int = mesh.getNumVertices()
		val vertices = FloatArray(count * stride)
		mesh.getVertices(start * stride, count * stride, vertices)
		// getVertices(0, vertices.length, vertices);
		Mesh.transform(matrix, vertices, stride, posOffset, numComponents, 0, count)
		// setVertices(vertices, 0, vertices.length);
		mesh.updateVertices(start * stride, vertices)
	}

	fun transform(mesh: Mesh, matrix: Matrix4?, start: Int, count: Int) {
		val posAttr: VertexAttribute = mesh.getVertexAttribute(VertexAttributes.Usage.Position)
		val posOffset = posAttr.offset / 4
		val stride: Int = mesh.getVertexSize() / 4
		val numComponents = posAttr.numComponents
		val numVertices: Int = mesh.getNumVertices()
		val vertices = FloatArray(count * stride)
		mesh.getVertices(start * stride, count * stride, vertices)
		// getVertices(0, vertices.length, vertices);
		Mesh.transform(matrix, vertices, stride, posOffset, numComponents, 0, count)
		// setVertices(vertices, 0, vertices.length);
		mesh.updateVertices(start * stride, vertices)
	}

	fun removeEmptyItems(asset: SceneAsset): SceneAsset {
		asset.scene.model.nodes.toList().forEach {
			if (!it.hasChildren() && it.parts.isEmpty) {
				asset.scene.model.nodes.removeValue(it, true)
			}
		}
		asset.scene.cameras.clear()
		asset.scene.lights.clear()
		return asset
	}

	fun materialToRoot(asset: SceneAsset): SceneAsset {
		val model = asset.scene.model

		if (model.materials.isEmpty) {
			val materials = mutableSetOf<Material>()
			model.nodes.forEach {
				it.parts.forEach {
					materials.add(it.material)
				}
			}
			materials.forEach {
				model.materials.add(it)
			}
		}

		return asset
	}
}