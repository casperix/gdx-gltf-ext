package casperix.gltf.loader

import casperix.gltf.model.ModelUtil
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.model.Node
import com.badlogic.gdx.math.Quaternion
import com.badlogic.gdx.math.Vector3
import net.mgsx.gltf.loaders.gltf.GLTFLoader
import net.mgsx.gltf.scene3d.scene.SceneAsset


object GLTFLoaderExt {
	fun load(path: String, mergeMeshes: Boolean = true, removeEmpty: Boolean = true, materialToRoot:Boolean = true): SceneAsset {
		val file = Gdx.files.internal(path)
		return load(file, mergeMeshes, removeEmpty, materialToRoot)
	}

	fun load(file:FileHandle, mergeMeshes: Boolean = true, removeEmpty: Boolean = true, materialToRoot:Boolean = true): SceneAsset {
		if (!file.exists())
			throw Error("File not exist: ${file.path()}")

		var asset =try {
			 GLTFLoader().load(file)
		} catch (cause: Throwable) {
			throw Error("Error in loading ${file.path()}", cause)
		}

		if (mergeMeshes) {
			asset = ModelUtil.mergeAssetModel(asset)
		}
		if (removeEmpty) {
			asset = ModelUtil.removeEmptyItems(asset)
		}
		if (materialToRoot) {
			ModelUtil.materialToRoot(asset)
		}
		return asset
	}

	fun loadModel(path: String, mergeMeshes: Boolean = true, removeEmpty: Boolean = true): Model {
		return load(path, mergeMeshes, removeEmpty).scene.model
	}

	fun createAdapter(model: Model): Node {
		val root = Node()

		model.nodes.forEach { node ->
			if (node.parent == null) {
				root.addChild(node)
			}
		}

		model.nodes.clear()
		model.nodes.add(root)

		return root
	}

	fun scaleAdapter(model: Model, scale: Float): Node {
		val root = createAdapter(model)
		root.scale.set(Vector3(scale, scale, scale))
		root.calculateTransforms(true)
		return root
	}

	fun fbxAdapter(model: Model, scale: Float = 0.001f, rotate: Boolean = true): Model {
		val root = Node()

		model.nodes.forEach { node ->
			if (node.parent == null) {
				root.addChild(node)
			}
		}

		root.scale.set(Vector3(scale, scale, scale))
		if (rotate) {
			root.rotation.set(Quaternion().set(Vector3.X, 90f))
		}
		root.calculateTransforms(true)

		model.nodes.clear()
		model.nodes.add(root)

		return model
	}

}