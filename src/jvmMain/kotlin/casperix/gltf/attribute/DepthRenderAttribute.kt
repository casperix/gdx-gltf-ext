package casperix.gltf.attribute

import com.badlogic.gdx.graphics.g3d.Attribute

class DepthRenderAttribute() : Attribute(ID) {
	companion object {
		val ID = register("shadowRender")
	}

	override fun compareTo(other: Attribute): Int {
		val delta = other.type - this.type
		return delta.toInt()
	}

	override fun copy(): Attribute {
		return DepthRenderAttribute()
	}

}