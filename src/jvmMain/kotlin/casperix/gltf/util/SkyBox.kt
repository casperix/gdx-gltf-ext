package casperix.gltf.util

import casperix.misc.DisposableHolder
import casperix.gltf.render.RenderManager
import casperix.gltf.scene.RenderableGroup
import casperix.signals.then
import com.badlogic.gdx.graphics.Cubemap
import net.mgsx.gltf.scene3d.scene.SceneSkybox

class SkyBox(renderManager: RenderManager, private val environmentCubeMap:Cubemap) : DisposableHolder() {
	private val skyBox: SceneSkybox

	init {
		// setup skybox
		skyBox = SceneSkybox(environmentCubeMap)
		renderManager.add(skyBox, RenderableGroup(true, false, 16))

		renderManager.nextFrame.then(components) {
			skyBox.update(renderManager.camera, it.toFloat())
		}
	}

	override fun dispose() {
		skyBox.dispose()
		environmentCubeMap.dispose()
	}

}