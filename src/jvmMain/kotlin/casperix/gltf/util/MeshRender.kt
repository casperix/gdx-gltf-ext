package casperix.gltf.util

import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.RenderableProvider
import com.badlogic.gdx.graphics.g3d.Shader
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Pool

class MeshRender(var mesh: Mesh, var transform: Matrix4 = Matrix4(), var material: Material = Material(), var shader: Shader? = null, var primitiveType: Int = GL20.GL_TRIANGLES) : RenderableProvider {
	override fun getRenderables(renderables: Array<Renderable>?, pool: Pool<Renderable>?) {
		val item = pool!!.obtain()!!
		item.material = material
		item.shader = shader
		item.worldTransform.set(transform)

		item.meshPart.set("", mesh, 0, if (mesh.numIndices != 0) mesh.numIndices else mesh.numVertices, primitiveType)
		renderables!!.add(item)
	}
}