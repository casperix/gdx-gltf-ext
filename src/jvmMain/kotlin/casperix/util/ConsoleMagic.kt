package casperix.util

object ConsoleMagic {
//	https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
	val red = "\u001b[31m"
	val white = "\u001b[37m"
	val reset = "\u001b[0m"

	fun makeRed(value: String): String {
		return "$red$value$reset"
	}

	fun makeWhite(value: String): String {
		return "$white$value$reset"
	}
}