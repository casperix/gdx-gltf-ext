package com.badlogic.gdx.graphics.glutils

import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture

class FrameBufferTextureArrayBuilder(width: Int, height: Int, val layerAmount: Int, val format: Pixmap.Format, val hasDepth: Boolean, val hasStencil: Boolean,
									 val minFilter: Texture.TextureFilter, val magFilter: Texture.TextureFilter, val uWrap: Texture.TextureWrap, val vWrap: Texture.TextureWrap) : GLFrameBuffer.GLFrameBufferBuilder<FrameBufferTextureArray>(width, height) {
	init {
		addBasicColorTextureAttachment(format)
		if (hasDepth) addBasicDepthRenderBuffer()
		if (hasStencil) addBasicStencilRenderBuffer()
	}

	override fun build(): FrameBufferTextureArray {
		return FrameBufferTextureArray(this)
	}
}