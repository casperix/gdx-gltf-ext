out vec4 FragColor;

uniform sampler2D u_albedo_texture;
uniform sampler2D u_normal_texture;
uniform sampler2D shadowMap;
uniform vec3 toLightDirection;
uniform float bias;

varying mat3 v_normal_mat;

const float gammaInverted = 1.0 / 2.2;

in VS_OUT {
    vec3 position;
    vec3 normal;
    vec2 texCoord;
    vec4 positionInLightSpace;
    vec4 color;
} fs;

float getShadow(vec3 shadowProjection, vec2 offset, float bias)
{
    float closestDepth = texture(shadowMap, offset+shadowProjection.xy).r;
    float currentDepth = shadowProjection.z;
    return currentDepth - bias > closestDepth && currentDepth - bias  <= 1.0  ? 1.0 : 0.0;
}

float getPCFShadow(vec3 shadowProjection, float bias) {
    vec2 t = 1.0 / textureSize(shadowMap, 0);
    float shadow = 0;

    shadow += getShadow(shadowProjection, vec2(0, 0) * t, bias);
    shadow += getShadow(shadowProjection, vec2(-1, 0) * t, bias);
    shadow += getShadow(shadowProjection, vec2(1, 0) * t, bias);
    shadow += getShadow(shadowProjection, vec2(0, -1) * t, bias);
    shadow += getShadow(shadowProjection, vec2(0, 1) * t, bias);

    return shadow / 5.0;
}

void main()
{
    float facingFactor = float(gl_FrontFacing) * 2.0 - 1.0;

    #ifdef NORMAL_TEXTURE
    mat3 normal_mat = v_normal_mat;
    normal_mat[2] = normal_mat[2] * facingFactor;
    vec3 normal = 2.0 * texture(u_normal_texture, fs.texCoord).xyz - 1.0;
    normal = normalize(normal_mat*normal);
    #else
    vec3 normal = normalize(fs.normal);
    #endif

    #ifdef DIFFUSE_TEXTURE
    vec4 sourceColor = texture(u_albedo_texture, fs.texCoord);
    #else
    vec4 sourceColor = vec4(0.8, 0.8, 0.8, 1.0);
    #endif

    #ifdef VERTEX_COLOR
    sourceColor *= fs.color;
    #endif

    #ifdef ALPHA_BLEND_TEXTURE
    if (sourceColor.a < 0.2) discard;
    #endif

    #ifdef RECEIVE_SHADOW
    vec3 shadowProjection = fs.positionInLightSpace.xyz / fs.positionInLightSpace.w* 0.5 + 0.5;
    #ifdef PCF_SHADOW
    float shadow = getPCFShadow(shadowProjection, bias);
    #else
    float shadow = getShadow(shadowProjection, vec2(0.0), bias);
    #endif//PCF_SHADOW
    #else
    float shadow = 0;
    #endif//SHADOW

    #ifdef CALCULATE_LIGHT
    float ambientWeight = 0.3;
    float light_factor = ambientWeight + (1 - shadow) * (1 - ambientWeight) * max(0.0, dot(normal, toLightDirection));
    vec3 outputColor = sourceColor.rgb * light_factor;
    #else
    vec3 outputColor = sourceColor.rgb;
    #endif

    #ifdef HDR_CORRECTION
    outputColor = outputColor / (outputColor + vec3(1.0));
    #endif

    #ifdef GAMMA_CORRECTION
    outputColor = pow(outputColor, vec3(gammaInverted));
    #endif

    FragColor = vec4(outputColor, sourceColor.a);
}
