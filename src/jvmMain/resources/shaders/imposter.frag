#ifdef GL_ES
precision mediump float;
#endif

#ifdef shadowMapFlag
uniform float u_shadowBias;
uniform sampler2D u_shadowTexture;
uniform float u_shadowPCFOffset;

in vec3 v_shadowMapUv;

float getShadowness(vec2 offset)
{
    const vec4 bitShifts = vec4(1.0, 1.0 / 255.0, 1.0 / 65025.0, 1.0 / 16581375.0);
    return step(v_shadowMapUv.z, dot(texture2D(u_shadowTexture, v_shadowMapUv.xy + offset), bitShifts) + u_shadowBias);// (1.0/255.0)
}

float getShadow()
{
    return (//getShadowness(vec2(0,0)) +
    getShadowness(vec2(u_shadowPCFOffset, u_shadowPCFOffset)) +
    getShadowness(vec2(-u_shadowPCFOffset, u_shadowPCFOffset)) +
    getShadowness(vec2(u_shadowPCFOffset, -u_shadowPCFOffset)) +
    getShadowness(vec2(-u_shadowPCFOffset, -u_shadowPCFOffset))) * 0.25;
}
    #endif//shadowMapFlag

in vec2 v_tex;
in float v_layer;

uniform sampler2DArray u_textureArray;


void main()
{
    #ifdef shadowMapFlag
    float shadow = 0.2 + 0.8 * getShadow();
    #else
    float shadow = 1.0;
    #endif

    float minLayer = floor(v_layer);
    float maxLayer = floor(v_layer + 1.0);
    float maxWeight = v_layer - minLayer;
    float minWeight = 1.0 - maxWeight;
//    vec4 textureColor = (shadow * 0.5 + 0.5) * texture(u_textureArray, vec3(v_tex, minLayer)) * (minWeight) + texture(u_textureArray, vec3(v_tex, maxLayer)) * (maxWeight);
    vec4 textureColor = texture(u_textureArray, vec3(v_tex, v_layer));
    if (textureColor.a < 0.03) discard;
    gl_FragColor = textureColor;
}