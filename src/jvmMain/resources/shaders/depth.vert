uniform mat4 lightModelMatrix;

in vec3 a_position;
in vec2 a_texCoord0;

varying vec2 v_texCoord;

void main()
{
    v_texCoord = a_texCoord0;
    gl_Position =  lightModelMatrix*vec4(a_position, 1.0);
}